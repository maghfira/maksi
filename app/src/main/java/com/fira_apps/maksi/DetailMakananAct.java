package com.fira_apps.maksi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class DetailMakananAct extends AppCompatActivity {
    private ImageView iv_makanan;
    private TextView tv_deskripsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_makanan);

        iv_makanan = findViewById(R.id.iv_makanan);
        tv_deskripsi = findViewById(R.id.tv_deskripsi);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setActionBarTitle(getIntent().getStringExtra("nama_makanan") + " - " + getIntent().getStringExtra("asal_daerah"));
        Glide.with(getApplicationContext())
                .load(getIntent().getIntExtra("gambar", 0))
                .into(iv_makanan);
        tv_deskripsi.setText(getIntent().getStringExtra("deskripsi"));
    }

    public void setActionBarTitle(String nama_makanan) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(nama_makanan);
        }
    }
}
