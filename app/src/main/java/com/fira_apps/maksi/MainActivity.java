package com.fira_apps.maksi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.fira_apps.maksi.Adapter.ListMakananAdapter;
import com.fira_apps.maksi.Data.DataMakanan;
import com.fira_apps.maksi.Model.Makanan;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rv_makanan;
    private ArrayList<Makanan> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv_makanan = findViewById(R.id.rv_makanan);
        rv_makanan.setHasFixedSize(true);

        list.addAll(DataMakanan.getListData());
        showListMakanan();
    }

    private void showListMakanan() {
        rv_makanan.setLayoutManager(new LinearLayoutManager(this));
        ListMakananAdapter listMakananAdapter = new ListMakananAdapter(list);
        rv_makanan.setAdapter(listMakananAdapter);
        listMakananAdapter.setOnItemClickCallback(new ListMakananAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(Makanan data) {
                showDetailMakanan(data);
            }
        });
    }

    private void showDetailMakanan(Makanan makanan) {
        Intent gotodetailmakanan = new Intent(MainActivity.this, DetailMakananAct.class);
        gotodetailmakanan.putExtra("gambar", makanan.getGambar());
        gotodetailmakanan.putExtra("nama_makanan", makanan.getNama_makanan());
        gotodetailmakanan.putExtra("asal_daerah", makanan.getAsal_daerah());
        gotodetailmakanan.putExtra("deskripsi", makanan.getDeskripsi());
        startActivity(gotodetailmakanan);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_about) {
            Intent gotoabout = new Intent(MainActivity.this, AboutAct.class);
            startActivity(gotoabout);
        }
        return super.onOptionsItemSelected(item);
    }
}
